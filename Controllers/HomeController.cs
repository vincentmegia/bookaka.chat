﻿using System.Web.Mvc;

namespace Bookaka.Chat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Chat");
        }

        public ActionResult Chat()
        {
            return View();
        }
    }
}