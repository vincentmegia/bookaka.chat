﻿$(function () {
    // Reference the auto-generated proxy for the hub.
    var chat = $.connection.chatHub;
    var joinedGroupName = "";
    // Create a function that the hub can call back to display messages.
    //chat.client.PublishMessage = function (name, message) {
    chat.client.addNewMessageToPage = function (name, message) {
        // Add the message to the page.
        var currentdate = new Date();
        var datetime = currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds();

        //$('#discussion').append('<li><strong>' + datetime + ' ' + htmlEncode(name)
        //    + '</strong>: ' + htmlEncode(message) + '</li>');
        //$('#discussion').val(function (i, text) {
        //    return text + '[' + htmlEncode(name) + '][' + datetime + ']: ' + htmlEncode(message);
        //});
        $('#discussion').val($('#discussion').val() + " " + '[' + htmlEncode(name) + '][' + datetime + ']: ' + htmlEncode(message) + String.fromCharCode(13, 10));
    };
    // Get the user name and store it to prepend to messages.
    $('#displayname').val(prompt('Enter your name:', ''));
    $('#chatName').text($('#displayname').val());
    // Set initial focus to message input box.
    $('#message').focus();
    // Start the connection.
    $.connection.hub.start().done(function () {
        $('#sendmessage').click(function () {
            // Call the Send method on the hub.
            //chat.server.send($('#displayname').val(), $('#message').val());
            chat.server.publishMessage(joinedGroupName, $('#displayname').val(), $('#message').val());
            // Clear text box and reset focus for next comment.
            $('#message').val('').focus();
        });

        $('#fomoChat')
            .click(function () {
                chat.server.joinGroup("fomoChat");
                $('#joinedGroupName').text('FOMO Chat');
                joinedGroupName = "fomoChat";
            });
        $('#boboChat')
            .click(function () {
                chat.server.joinGroup("boboChat");
                $('#joinedGroupName').text('BOBO Chat');
                joinedGroupName = "boboChat";
            });
    });

});
// This optional function html-encodes messages for display in the page.
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}