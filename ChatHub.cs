﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace Bookaka.Chat
{
    public class ChatHub : Hub
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="name"></param>
        /// <param name="message"></param>
        public void PublishMessage(string groupName, string name, string message)
        {
            Clients.Group(groupName).addNewMessageToPage(name, message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public Task JoinGroup(string groupName)
        {
            return Groups.Add(Context.ConnectionId, groupName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public Task LeaveGroup(string groupName)
        {
            return Groups.Remove(Context.ConnectionId, groupName);
        }
    }
}